<?php

return [
    'adminEmail' => 'admin@example.com',
    'socketio' => [
        'ioPort' => 8890,
        'ioHost' => 'http://127.0.0.1',
        'webPort' => 8891,
        'webHost' => 'http://0.0.0.0',
    ],
];
