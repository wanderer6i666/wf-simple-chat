<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Message extends \yii\redis\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'message'], 'filter', 'filter' => 'strip_tags'],
            [['name', 'message'], 'filter', 'filter' => 'trim'],
            [['name', 'message'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 7],
            [['message'], 'string', 'max' => 1000],
            [['name', 'message'], 'required'],
            [['created_at', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return ['id', 'name', 'message', 'color', 'created_at'];
    }
}