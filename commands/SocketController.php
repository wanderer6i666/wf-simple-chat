<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\components\Worker;
use Workerman\WebServer;
use Workerman\Autoloader;
use PHPSocketIO\SocketIO;
use app\models\Message;

class SocketController extends Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::setAlias('@webroot', Yii::getAlias('@app/web'));
        parent::init();
    }

    /**
     * Start socket server.
     * @param srting $flag
     */
    public function actionStart($flag = null)
    {
        $this->runWeb();
        $this->runIo();

        $this->executeWorkerCommand('start', $flag);
    }

    public function runWeb()
    {   $host = Yii::$app->params['socketio']['webHost'].':'.Yii::$app->params['socketio']['webPort'];
        $web = new WebServer($host);
        $web->addRoot('localhost', Yii::getAlias('@webroot'));
    }

    /**
     * Chat handler.
     */
    public function runIo()
    {
        $io = new SocketIO(Yii::$app->params['socketio']['ioPort']);
        $io->on('connection', function($socket) {
            $socket->addedUser = false;

            // when the client emits 'new message', this listens and executes
            $socket->on('new message', function ($data) use ($socket) {
                // Save message.
                $message = new Message;
                $message->attributes = [
                    'name' => $data['username'],
                    'message' => $data['message'],
                    'color' => $data['color'],
                ];

                if ($message->validate() && $message->save()) {
                    // we tell the client to execute 'new message'
                    $socket->broadcast->emit('new message', array(
                        'username' => $message->name,
                        'message' => $message->message,
                    ));
                } else {
                    if ($message->errors) {
                        foreach ($message->errors as $errors) {
                            foreach ($errors as $error) {
                                $socket->emit('error message', array(
                                    'message' => $error,
                                ));
                            }
                        }
                    } else {
                        $socket->emit('error message', array(
                            'message' => 'Error on message send.',
                        ));
                    }
                }
            });

            // when the client emits 'add user', this listens and executes
            $socket->on('add user', function ($username) use ($socket) {
                global $usernames, $numUsers;
                // we store the username in the socket session for this client
                $socket->username = $username;
                // add the client's username to the global list
                $usernames[$username] = $username;
                ++$numUsers;
                $socket->addedUser = true;
                $socket->emit('login', array(
                    'numUsers' => $numUsers
                ));
                // echo globally (all clients) that a person has connected
                $socket->broadcast->emit('user joined', array(
                    'username' => $socket->username,
                    'numUsers' => $numUsers
                ));
            });

            // when the client emits 'typing', we broadcast it to others
            $socket->on('typing', function () use ($socket) {
                $socket->broadcast->emit('typing', array(
                    'username' => $socket->username
                ));
            });

            // when the client emits 'stop typing', we broadcast it to others
            $socket->on('stop typing', function () use ($socket) {
                $socket->broadcast->emit('stop typing', array(
                    'username' => $socket->username
                ));
            });

            // when the user disconnects.. perform this
            $socket->on('disconnect', function () use ($socket) {
                global $usernames, $numUsers;
                // remove the username from global usernames list
                if($socket->addedUser) {
                    unset($usernames[$socket->username]);
                    --$numUsers;

                   // echo globally that this client has left
                   $socket->broadcast->emit('user left', array(
                       'username' => $socket->username,
                       'numUsers' => $numUsers
                    ));
                }
           });

        });
    }

    /**
     * Stop daemon.
     * @param srting $flag
     */
    public function actionStop($flag = null)
    {
        $this->executeWorkerCommand('stop', $flag);
    }

    /**
     * Restart daemon.
     * @param srting $flag
     */
    public function actionRestart($flag = null)
    {
        $this->executeWorkerCommand('restart', $flag);
    }

    /**
     * Reload daemon.
     * @param srting $flag
     */
    public function actionReload($flag = null)
    {
        $this->executeWorkerCommand('reload', $flag);
    }

    /**
     * Status daemon.
     * @param srting $flag
     */
    public function actionStatus($flag = null)
    {
        $this->executeWorkerCommand('status', $flag);
    }

    /**
     * Connections daemon.
     * @param srting $flag
     */
    public function actionConnections($flag = null)
    {
        $this->executeWorkerCommand('connections', $flag);
    }

    /**
     * Daemon commands info.
     */
    public function actionInfo()
    {
        $this->executeWorkerCommand();
    }

    /**
     * Execute worker command.
     *
     * @param srting $command
     * @param srting $flag
     */
    public function executeWorkerCommand($command = null, $flag = null)
    {
        Worker::$command = $command;
        Worker::$commandFlag = $flag;
        Worker::runAll();
    }
}