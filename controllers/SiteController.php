<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Message;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $messages = Message::find()->orderBy('created_at DESC')->limit(20)->all();

        return $this->render('index', [
            'messages' => array_reverse($messages),
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionLoadMoreMessages()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не найдена.'));
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $lastId = (int)Yii::$app->request->post('lastId');
        $messages = Message::find()->where(['between', 'id', 0, $lastId - 1])->orderBy('created_at DESC')->limit(20)->all();

        if ($messages) {
            return [
                'messages' => $this->renderPartial('parts/_messages', ['messages' => array_reverse($messages)]),
            ];
        } else {
            return [
                'messages' => '',
            ];
        }
    }
}
