Simple socket chat based on Yii 2
============================
~~~
Технологии:

    Apache v2.4
    PHP v7.0
    Redis v4
    Npm v5.3
    Socket.io
    Yii2 Framework
    Ubuntu 16.10
~~~

НАСТРОЙКА
------------

### 1) Установить NodeJS 
~~~
https://nodejs.org/en/download/
~~~

### 2) Установить Redis 
~~~
https://redis.io/topics/quickstart
~~~

### 3) Если нужно помеять конфигурацию Redis
~~~
config/console.php и config/web.php

return [
    //....
    'components' => [
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 1,
        ],
    ]
];
~~~

### 4) Если нужно помеять настройки портов
~~~
config/params.php


return [
    //....
    'socketio' => [
        'ioPort' => 8890,
        'ioHost' => 'http://127.0.0.1',
        'webPort' => 8891,
        'webHost' => 'http://0.0.0.0',
    ],
];

~~~

### 5) Выполнить "composer install"

### 6) Запустить сокет сервер
~~~
php yii socket/start

Доступные команды:
- socket                       
    socket/connections         Connections daemon.
    socket/info                Daemon commands info.
    socket/reload              Reload daemon.
    socket/restart             Restart daemon.
    socket/start               Start socket server.
    socket/status              Status daemon.
    socket/stop                Stop daemon.

Дополнительная справка:
php yii socket/info
~~~