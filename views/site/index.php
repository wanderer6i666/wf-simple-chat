<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';

$ioPort = Yii::$app->params['socketio']['ioHost'].':'.Yii::$app->params['socketio']['ioPort'];

$js = <<<JS
    var ioPort = '$ioPort';
JS;
$this->registerJs($js, \yii\web\View::POS_HEAD);

?>

<ul class="pages">
    <li class="chat page">
        <div class="chatArea">
            <ul class="messages">
                <?= $this->render('parts/_messages', ['messages' => $messages]) ?>
            </ul>
        </div>
        <?= Html::textInput('message', null, [
            'class' => 'inputMessage',
            'placeholder' => 'Write message..'
        ]) ?>
    </li>
    <li class="login page">
        <div class="form">
            <h3 class="title">What's your nickname?</h3>
            <?= Html::textInput('name', null, [
                'class' => 'usernameInput',
                'placeholder' => 'Type here..'
            ]) ?>
        </div>
    </li>
</ul>