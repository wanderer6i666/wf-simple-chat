<?php foreach ($messages as $message): ?>
    <li class="message" data-id="<?= $message->id ?>">
        <span class="username" style="color: <?= $message->color ?>;">
            <?= $message->name ?>
        </span>
        <span class="messageBody"><?= $message->message ?></span>
    </li>
<?php endforeach ?>